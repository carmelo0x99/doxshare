---
title: "2. The Cybersecurity Industry"
date: 2023-01-01T18:57:53+01:00
draft: false
tags: [cybersecurity, industry, ecosystem]
categories: [""]
---

### In this article, we will break down common domains in cybersecurity.
Cybersecurity is becoming a broader field as more industries migrate onto the Internet and become a part of the digital landscape. That means there are cybersecurity needs in nearly every industry in addition to cybersecurity being an industry itself. Security roles are no longer just the hacker stereotype of cracking into systems and writing code all the time; cybersecurity as a whole encompasses many skills that work together.

In this article, we’ll break down some of the big domains in the cybersecurity industry. There are lots of overlap between different domains and cybersecurity careers, so keep in mind that the domains are not drawn with hard lines, especially as they keep evolving!

## Security engineering
This section refers to the technical implementation of various forms of security.
- [Information security](https://www.cisco.com/c/en/us/products/security/what-is-information-security-infosec.html), or **InfoSec**, protects data in any form from being accessed, modified, shared, or deleted by the wrong people.
- [Network security](https://www.cisco.com/c/en/us/products/security/what-is-network-security.html?dtid=osscdc000283) is concerned with the network infrastructure of an organization that guards against unauthorized access or data from being intercepted.
- [Application security](https://www.cisco.com/c/en/us/solutions/security/application-first-security/what-is-application-security.html) refers to implementing measures that defend an application (mobile, desktop, or web) from attack, including both software and hardware solutions. Examples of application security include secure coding, the use of antivirus programs, firewalls, and encryption.
- [Cloud security](https://www.mcafee.com/enterprise/en-us/security-awareness/cloud.html) refers to the new field of making sure resources uploaded into the cloud are secure. Companies and users are constantly moving more resources into the cloud, and professionals in this field need to be familiar with implementing security in this environment.
- [Cryptography](https://www.kaspersky.com/resource-center/definitions/what-is-cryptography) focuses on methods to hide and un-hide information so that data is only readable or usable by authorized people. This requires familiarity with all types of encryption and hashing algorithms.
- [Critical infrastructure security](https://www.forcepoint.com/cyber-edu/critical-infrastructure-protection-cip) is defending physical systems that are becoming more digital/networked, such as energy grids, hospitals, water and waste systems, and even schools. Among the issues that come up are natural disasters and outages.

## Governance and compliance
It’s critical to understand international, federal, and state laws and regulations for security. This has implications on the security operations for all organizations. Compliance refers to _making sure an organization enforces certain policies, and continuously auditing as well_.

This is becoming an increasingly important area of work. While these roles might not require programming knowledge, these roles require foundational knowledge of cybersecurity as well as all the laws and regulations that impact a particular industry.

## Risk management and threat intelligence
No system will ever be perfect, and there will always be **risk**, so this area of work is about managing that risk. Cyber risk management is an ongoing process of identifying, analyzing, and remediating any organizations' cybersecurity threats.

Some of the key components include:
- Development of policies and procedures
- Identification of emerging risks
- Testing of the overall security posture
- Documentation of vendor risk management.

Risk is managed through:
- identifying risks,
- assessing the likelihood and potential threat of security vulnerabilities, and
- finding the most cost-effective and efficient security measures.

Threat intelligence is the continuous gathering of knowledge of possible attacks. Intelligence could look like knowing the motivations behind attacks, what the scale of attacks could be, and what vectors that might use. These roles often intersect with **data science** and **machine learning** because of the need to process all this information.

## Security operations
People who work in this area are responsible for implementing security principles, monitoring for incidents, and recovering from disasters. They work closely with everyone under the security umbrella to:
- Detect when something has gone wrong
- Implement preventative measures against cyber attacks
- Make sure there are back-ups in case a system is compromised and data is lost
- Track changes to a system
- Come up with disaster recovery plans in advance
- Create documents and organization policies for all of the above.

## Education
Security education is a growing area in itself! This domain acknowledges that the most securely designed technologies are only as strong as the people who use them. User education teaches best practices for people to protect themselves against cyber threats. Security training also happens in large organizations, where employees are educated and updated on the organization’s security policies and practices.

This domain can also include the career development and training of new security professionals as well.

## Conclusion
There are a broad set of cybersecurity roles that vary in technical expertise and required training. There are roles that intersect with engineering, roles that intersect with education, and roles that intersect with administration and management.

Research is to be done with an open mind.

