---
title: "3. Introducing Cybersecurity History & Standards"
date: 2023-01-02T11:07:00+01:00
draft: false
tags: [cybersecurity,history,standards]
categories: [""]
---

Cybersecurity was invented as soon as the first group of people attempted to defend from digital attacks. We’ll walk through a brief history of cybersecurity through the 60’s, 70’s, all the way to the 2010’s and the present. The evolution of defensive techniques mirrors the evolution of attack techniques, making this history rife with interesting challenges. One can see in the graph below just how quickly cybersecurity needs to keep up with attacks.

![Graph](/images/3_graph.png)

## Becoming familiar with standards and regulations
With laws and industry-specific policies, companies and organizations that hold our data are held responsible for protecting our data and thus, our privacy. This sets a balanced expectation of what is personal security practice and what should be built into the tools we use every day.

Without enforcing that trust, we would not be able to carry out sensitive or financial transactions on the web. It would be incredibly difficult to coordinate the training of security professionals. We’ll talk about some widely implemented security frameworks, what constitutes cybercrime, and laws and policies that deal with a specific industry like healthcare or finance.

### Links
- [List of security hacking incidents](https://en.wikipedia.org/wiki/List_of_security_hacking_incidents)

