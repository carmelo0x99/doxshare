---
title: "11. Zero-Day & DDoS Attacks"
date: 2023-01-02T17:23:13+01:00
draft: false
tags: [""]
categories: [""]
---

### Analysis about zero-day and DDoS attacks as well as some of the strategies they use.
This article will introduce two unique threats to organizations: zero-day and DDoS attacks. These are threats that enterprises must understand and protect themselves against.

### Zero-Day Attacks
A “zero-day” (also called “0-day”) vulnerability is a newly-discovered software bug that a developer was not aware of before the software was released. Therefore, after it is discovered, the developer has “zero” days to patch it before it can be exploited. When a “zero-day attack” occurs, the vulnerability quickly becomes known and is patched by the developer.

For example, in one well-known case, malware referred to as Stuxnet was used to attack Iran’s nuclear program. [Using four separate zero-days](https://www.zdnet.com/article/stuxnet-attackers-used-4-windows-zero-day-exploits/), the Stuxnet worm ruined up to one-fifth of Iran’s Nuclear Centrifuges. The malware was widely believed to have been developed jointly by the United States and Israel. Zero-day attacks were also responsible for the [2014 attack on Sony pictures](https://www.vox.com/2015/1/20/11557888/heres-what-helped-sonys-hackers-break-in-zero-day-vulnerability) and [Russia’s 2016 attack on the DNC](https://www.washingtonpost.com/world/national-security/russian-government-hackers-penetrated-dnc-stole-opposition-research-on-trump/2016/06/14/cf006cb4-316e-11e6-8ff7-7b6c1998b7a0_story.html).

Because zero-days are difficult to discover and exploit, they are more often leveraged by nation-state actors, who have the resources and infrastructure to find and exploit these vulnerabilities. Although rare, [new attacks occur](https://googleprojectzero.blogspot.com/) each year.

### Finding and Classifying Vulnerabilities
The [vast majority of cyber attacks](https://googleprojectzero.blogspot.com/2020/07/detection-deficit-year-in-review-of-0.html) exploit existing vulnerabilities. These vulnerabilities are catalogued and numbered as CVEs, or “Common Vulnerabilities and Exposures” and are maintained in places like [the Mitre Corporation’s database](https://cve.mitre.org/) or the [National Vulnerability Database](https://nvd.nist.gov/) (NVD).

<!--An image showing a CVE report CVE-2020-27918. It was for "A use after free issue was addressed with improved memory management".-->

Thousands of CVEs are recorded [every year](https://www.makeuseof.com/how-many-security-vulnerabilities/) and are typically found first by either security companies or researchers participating in companies’ bug bounty programs. Bug bounty programs offer money (a bounty) to anyone able to find a vulnerability in their systems. Once the vulnerability is found, the organizations are able to implement a patch and release a software update as soon as possible. It is much more difficult to attack an organization that keeps its software up to date!

### DDoS
DDoS stands for Distributed Denial of Service. A DDoS attack is when an attacker attempts to make a resource, such as a website’s various servers, go offline by overwhelming it with web traffic. It is similar to trying to drive during rush hour: the more cars there are, the slower everyone goes. How does an attacker do this? They make requests to a resource with a large number of computers, overwhelming the resource and making it run slower and slower until eventually, it goes offline entirely.

Because an attacker must use a large number of computers, the attack is “distributed” across multiple devices. The goal is to knock the resource offline so that it “denies service”; hence the name “distributed denial of service”.

But where does an attacker get all of these computers from? Large websites are equipped to handle thousands of visits per day, so it takes a lot of web traffic to overwhelm them. This traffic comes from botnets. [Botnets](https://www.paloaltonetworks.com/cyberpedia/what-is-botnet) are “robot networks” made up of computers infected by malware. These botnets can be made up of millions of bots, and can even include IoT devices. A single attacker can spread malware to many devices, and then use all of those devices in concert to act together, oftentimes without the victims ever knowing that their devices are infected. 

Because botnet services can be rented out to other cybercriminals for specific attacks, anyone with the money to rent a botnet can carry out a DDoS attack, leveraging the power of millions of computers, making DDoS attacks [ever more powerful](https://www.cloudflare.com/learning/ddos/famous-ddos-attacks/).

### Types of DDoS Attacks
Network connections are defined by layers with each layer responsible for a different part of data transmission. Different types of DDoS attacks [target different network layers](https://us.norton.com/internetsecurity-emerging-threats-what-is-a-ddos-attack-30sectech-by-norton.html), specifically, layers 3, 4, and 7, known respectively as the Network, Transport, and Application layers. Different DDoS attacks target different layers in different ways.

For example, one attack that targets the application layer is called “HTTP Flooding”. This is because the attacker sends lots of [HTTP requests](https://www.codecademy.com/articles/http-requests) — the kind of requests your browser makes when you visit a webpage. In effect, it is like refreshing a website over and over again, making a server load content repeatedly until it becomes overwhelmed.

On the other hand, “SYN Flooding” targets the Transport layer by taking advantage of something called a TCP Handshake. Basically, it asks the server to wait for confirmation of a connection, but then never gives that confirmation. This is like a large group of people all asking the same person to hold something for them, but no one ever takes their item back, until the person holding everything eventually becomes overwhelmed.

You can read more about [different types of DDoS attacks here](https://www.cloudflare.com/learning/ddos/what-is-a-ddos-attack/).

![A hacker sends multiple requests to the same system, flooding it](/images/11_HTTP_Flooding.svg)

### Fighting DDoS
There are a number of ways that websites try to guard against DDoS attacks, for example, by rate-limiting: limiting the number of requests a server will accept in a single time. CAPTCHAs can also provide some protection. Theoretically, a CAPTCHA can determine whether a user is a human or a “bot”, allowing legitimate web traffic to attempt to log in while blocking malicious automated traffic.

In general, it is difficult to guard against DDoS attacks. This is why websites seek protection from organizations such as Cloudflare, which provides protection against DDoS attacks by sitting between the server and the client, and forwarding legitimate traffic to the server while hiding malicious traffic. However, the rise of Cloudflare and similar protective services has raised other ethical issues, as these services [can protect (and profit from)](https://www.huffpost.com/entry/cloudflare-cybersecurity-terrorist-groups_n_5c127778e4b0835fe3277f2f?tzs=) illicit and/or terrorist organizations, raising the question of whether all sites deserve equal treatment. 

### Conclusion
Zero-Day and DDoS attacks are each unique and significant threats that organizations have to protect themselves against, although they represent only two of the myriad dangers that businesses face in today’s world. As attackers become more creative and institutional, and as businesses migrate even more of their operations online, the kinds of threats facing them are only going to increase. This is why it is key to both understand the types of threats organizations encounter, and to understand the steps you can take to mitigate them. Cybersecurity is a dynamic and diverse field, but the greater your breadth of knowledge, the better placed you are to defend against evolving threats. 
