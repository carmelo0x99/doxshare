---
title: "6. Introduction to Cyber Threats"
date: 2023-01-02T12:00:34+01:00
draft: false
tags: [cybersecurity,threats]
categories: [""]
---

## Cyber Threats and the Threat Landscape
The threat landscape is ever-evolving and ever-expanding. The constant conflict between attackers and defenders pushes attackers to develop new attacks and defenders to develop new ways to stop those attacks. As new technology is developed, even more new cyber threats will enter the landscape.

The goal of this unit is to explore some current cyber threats, how to avoid threats them, and how to mitigate them. We will discuss some common threats, such as malware, SQL injections, phishing, cross-site scripting, and more!

How do we decide which threats are the most important to protect against? The CIA Triad gives us the principles of Confidentiality, Integrity, and Availability to use as a measure.
- **Confidentiality** is the principle of protecting private information and keeping it private. It should only be seen by users who should have access to it
- **Integrity** is the principle of protecting data from being changed or deleted by unauthorized parties
- **Availability** is the principle of data being consistently, reliably available to authorized users

Different cyber threats will threaten these principles. Keep these in mind as we explore the current threat landscape.

