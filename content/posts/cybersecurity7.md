---
title: "7. What Is Malware?"
date: 2023-01-02T12:38:07+01:00
draft: false
tags: [cybersecurity,malware]
categories: [""]
---

Malware is malicious software inserted into a system to cause damage to systems or data or to gain unauthorized access to a network.

Some examples of malware are:
- Viruses
- Adware
- Spyware
- Scareware
- Trojan horses
- Rootkits
- Ransomware
- Worms
- Fileless malware

### Malware: Viruses
A virus is a type of self-replicating malware that attaches itself to other programs and executables without the permission of the user.

### Malware: Worms
A worm is a type of self-replicating malware that copies itself from computer to computer without user intervention.
A worm could replicate so much that it overloads the host. By doing this, the worm could bring down the system and violate availability.

### Malware: Spyware
Spyware is malware downloaded without a user’s authorization which is used to steal sensitive information and relay it to an outside party in a way that harms the original user.

The key word here is “spy”. Clicking suspicious links or downloads could result in spyware.

### Malware: Adware
Adware is unwanted software designed to throw advertisements up on the screen. This malware is usually more annoying than dangerous.

### Social Engineering: Phishing
Phishing is a type of social engineering attack in which a threat actor, posing as a trustworthy source, attempts to trick a victim into doing something, either through email, webpages or even by phone.

#### How To Spot Phishing
How can a user spot phishing attacks? Look out for:
- Suspicious domains and email addresses
- Punctuation errors
- Typos
- Unusual scenarios
- Suspicious attachments or links
- Incorrect formatting

If something seems wrong, assume it is! Don’t be afraid to double-check and verify.

#### Phishing Uses
Phishing is a social engineering tactic that can be used for many things, such as stealing credentials or getting malware onto a system.

#### Specialized Types of Phishing
Some specialized types of phishing include:
- Vishing: “Voice” phishing uses spam calls
- Smishing: “SMS” phishing uses text messages
- Spear Phishing: A phishing strategy that targets specific victims
- Whaling: A phishing strategy that targets high-profile victims

### SQL Injection
A SQL injection is a serious vulnerability affecting applications that use SQL as their database language. Through cleverly constructed text inputs that modify the backend SQL query, threat actors can force the application to output private data or respond in ways that provide intel. SQL injections attacks can ultimately be used to steal information and even take complete control of a system.

#### Types of SQL Injections
SQL injections can be broken into multiple types depending on how information is retrieved: union-based, error-based, boolean-based, time-based, and out-of-band injections.

#### Mitigating SQL Injection Attacks: Input Sanitization
One way SQL injections can be mitigated is through input sanitization. Sanitization is the process of removing dangerous characters from user input.

    SELECT username, email
      FROM users
      WHERE id = '1' AND '1' = '2';

Dangerous characters might include:
- `’`
- `;`
- `\--`

This is important because they allow attackers to extend SQL queries to gain more information from a database.

Careful, this method is not the perfect defense against SQL injections. Removing characters may have no effect in some queries and, if an attacker finds a way to bypass the sanitization process, they can easily inject data into the victim's system.

#### Union-Based Injections
Uses the UNION SQL keyword to take two separate SELECT queries and combine their results. Consider the user input:

    soap' UNION SELECT username,password,NULL
      FROM user_table;-- -

Being added to the query:

    query = "SELECT product_name, product_cost, product_description
               FROM product_table
               WHERE product_name = " + USER_INPUT + "'";

Results in:

    SELECT product_name, product_cost, product_description
      FROM product_table
      WHERE product_name = 'soap' UNION
        SELECT username,password,NULL
        FROM user_table;-- -';

This resulting query would expose all the usernames and passwords of the users!

#### Error-Based Injections
An attacker writes a malicious SQL query to force the application to return an error message with sensitive data. The inside statement of the following SQL query gets the password for the profile ID 1 but throws an error on the value type that should be returned. This error accidentally gives away the password!

    SELECT user_id
      FROM users
      WHERE username='asdf' UNION
        select 1, exp(~(select*from(SELECT Password FROM profiles
          WHERE ID=1)x)); -- -

#### Boolean-Based Injections
An attacker takes note of the difference in web responses after sending SQL queries that result in either TRUE or FALSE. Depending on the result, the HTTP response will change or stay the same. Even though no data is returned from the database, the attacker can gain insight into the database (figure out table names) and eventually build up for a Union-based injection.

    SELECT username, email
      FROM users
      WHERE id = '1' AND '1' = '1';

#### Time-Based Injections
Makes use of several built-in SQL functions, such as `SLEEP()` and `BENCHMARK()`, to cause visible delays in an application’s response time. Like boolean-based injections, this is also used by an attacker to infer information about the database.

Consider the SQL query in the code block. If there’s a 5-second delay before a response from the server, an attacker can confirm the `admin` user’s password is `P@ssw0rd123`.

    SELECT id
      FROM users
      WHERE username = 'a' OR
      IF((SELECT password
          FROM users
          WHERE username='admin')='P@ssw0rd123', SLEEP(5), NULL);-- -';

#### Out-of-Band SQL Injections
Rare and difficult injections to execute for an attacker because the attacker is unable to use the same channel to send the SQL query and gather results. Generally, these SQL injections will cause the database server to send HTTP or DNS requests containing SQL query results to an attacker-controlled server. From there, the attacker could review the log files to identify the query results.

### Cross-Site Scripting (XSS)
Cross-Site Scripting (XSS) is a vulnerability that occurs when a web application returns unsanitized input to the front end of an application.

Three types of XSS attacks are:
- Stored XSS: when a server saves an attacker’s input into its datastores.
- Reflected XSS: when a user’s input is immediately returned back to the user.
- DOM-Based XSS: when user input is interpreted by the DOM, an attacker could inject arbitrary code.

The code shows examples of HTML tags that help attackers inject dangerous input.
```
    <script>alert(1);</script>
    <img src="X" onerror=alert(1);>
    <b onmouseover=alert(1)>click me!</b>
    <body onload=alert('test1')>
```

### Preventing Cross-Site Scripting
XSS can be mitigated by properly sanitizing input, as well as using specialized functions. We can generally succeed in preventing XSS attacks by removing potentially dangerous keywords or potentially dangerous characters such as:
- `<`
- `>`
- `"`
- `=`

Rather than remove characters, we could replace them with the HTML-encoded versions. For example, the `<` character would be converted to the “<” string.

### Cross-Site Request Forgery (CSRF)
Cross-Site Request Forgery is a serious vulnerability that results from poor session management. If the requests sent by an application aren’t unique, it’s possible for an attacker to craft a special request and send that to a user. If the user interacts with the crafted request, and sessions aren’t handled properly, an attacker may be able to assume the session identity of that user and carry out requests on their behalf.

In many cases of CSRF, a malicious actor crafts a URL embedded with a request like so:
```
http://bank.com/send?recipient=Stranger&amount=2000
```

