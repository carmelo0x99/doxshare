---
title: "5. Security Standards, Regulations, & Frameworks"
date: 2023-01-02T11:41:58+01:00
draft: false
tags: [cybersecurity,standards,regulations,frameworks]
categories: [""]
---

### This article is about standards, regulations, and frameworks that govern and support cybersecurity.
Cybersecurity is serious business; the potential consequences for a security breach can be dire, particularly if that breach targets sensitive information. In response, regulations and frameworks have been developed to ensure that the organizations which handle our data have a responsibility to do so securely and ethically, and have guidelines for how to do so.
- **Standards** and best practices are generally agreed-upon rules for maintaining security, generally within an organization. Standards may come as individual recommendations, or as part of a framework.
- **Regulations** are sets of standards that organizations must follow, generally due to legal obligation, and define the responsibilities organizations have.
- **Frameworks** are sets of standards and best practices that organizations can use to ensure their overall security. These are optional, but good practice for organizations to follow.

In this article, we will look at what constitutes cybercrime, discuss some US legislation setting standards for defense against cybercrime, and then discuss recommended security frameworks.

## Cybercrime
### What is Cybercrime?
Cybercrime is, generally speaking, any crime that makes use of or targets a computer. This is a broad definition because computers are versatile tools that can be used for good or evil. Without broad legal definitions, it’s difficult to prosecute new and innovative types of criminal activity.

When we think of cybercrime, we might first think of evil viruses on our computer and hackers getting into company data. What are some other types of cybercrime?
- **Extortion**: commonly in the form of ransomware, where attackers take control of a system and demand payment from the target.
- **Fraud**: a broad category including identity theft, scams, retail fraud, phishing, and more.
- **Theft**: stealing information during a data breach, or theft of services and resources, such as using other people’s computers to mine cryptocurrency.

Cybercrime does not always fit cleanly into a single category. For example, a malicious individual might break into an account using phishing, steal personal data, then threaten to publicly release it unless a payment is made. As with ‘ordinary’ crime, new types of cybercrime are being created all the time, limited only by the imagination of those carrying it out.

### Digital Forensics
Digital Forensics is the process of gathering the evidence of a cybercrime in a way that the evidence can be used in a court of law. It is a broad field with specializations, including:
- Disk Forensics: investigation of storage media such as hard drives.
- Memory Forensics: investigation of the live memory on a digital device.
- Network Forensics: investigation of network traffic.
- Mobile Forensics: investigation of mobile devices.
- Cloud Forensics: investigation of cloud environments.

The most important aspect of digital forensics is maintaining a chain of custody to prove that the data and evidence have not been modified or tampered with. Simply gathering evidence is not enough; the evidence must hold up to legal scrutiny in order to be useful, and conducting a forensic investigation incorrectly can destroy the original evidence.

## Regulations
### The Computer Fraud and Abuse Act
The Computer Fraud and Abuse Act (CFAA) is a US law that is used to prosecute cybercrime. At its core, the act makes it illegal to intentionally access a computer without authorization, or to access a computer in a way that exceeds authorization that has been granted. The broad scope of the CFAA means that it is used to prosecute a wide variety of cybercrime, including:
- Intrusion into networks and systems
- Malware attacks
- Theft of data and trade secrets
- Denial of service attacks

### Regulations For Organizations
Organizations like companies and public departments are subjected to additional legal obligations. These regulations make sure organizations act on security concerns to make their platforms as secure as possible. Many of us interact with and benefit from these regulations without even realizing it; information handled by doctors in the US are almost certainly protected by HIPAA.

In this section, we’ll be looking at three different sets of regulations; two of them are U.S federal laws, while the third is a contractional obligation created by the credit card industry. As we review the regulations and their requirements, think about how and why those requirements might have been created, and what they aim to prevent.

### HIPAA
The [Health Insurance Portability and Accountability Act](https://www.hhs.gov/hipaa/for-professionals/security/laws-regulations/index.html) (HIPAA) is a U.S. federal law designed to modernize various aspects of healthcare and insurance. More importantly for cybersecurity, it introduced regulations and standards requiring entities that handle **Protected Health Information** (PHI) to ensure its security and privacy.

Health and insurance entities have the following responsibilities:
- Ensure that the e-PHI is secure
- Protect against common threats that might target the e-PHI
- Prevent misuse of the e-PHI
- Inform everyone who works for them of these responsibilities

### GLBA
The [Gramm-Leach-Bliley Act](https://www.ecfr.gov/cgi-bin/text-idx?rgn=div5&node=16:1.0.1.3.38) (GLBA) is a U.S. federal law governing financial institutions, requiring them to protect sensitive customer information, and disclose how they share that information.

The cybersecurity section defines what steps financial institutions need to take to protect customer data. They have the following responsibilities:
- Ensure the security of customer information
- Protect against anticipated threats to the security of customer information
- Protect against anticipated misuse of customer information

### PCI DSS
The [Payment Card Industry Data Security Standard](https://www.pcisecuritystandards.org/) (PCI DSS, or PCI for short) is a regulation developed by credit card companies and governing merchants that handle credit card data.

The PCI DSS, as of 2021, outlines the following responsibilities for merchants:
- Build and maintain a secure network
- Protect cardholder data
- Maintain a vulnerability management program
- Implement strong access control measures
- Regularly monitor and test networks
- Maintain an information security policy

## The NIST Framework
The [NIST Cybersecurity Framework](https://www.nist.gov/cyberframework) is an optional framework published by NIST, originally developed to protect critical infrastructure in the US from cyber threats.

The NIST framework consists of five main elements or phases:
- **Identify** and understand the threats and risks the organization is likely to face.
- **Protect** the organization’s assets from those threats and risks.
- **Detect** incidents, such as cyberattacks or other events.
- **Respond** to incidents, preventing further damage.
- **Recover** from incidents, evaluating how to prevent reoccurrence cleaning up any damage that occurred.

Frameworks are not generally a one-size-fits-all solution to cybersecurity. Instead, they are a starting point for organizations to customize to their needs and threats. Frameworks and regulations are meant to work together, helping the organizations we rely on to remain secure, and protecting our information from those who would misuse it.

