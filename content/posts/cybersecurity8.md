---
title: "8. Phishing"
date: 2023-01-02T13:32:51+01:00
draft: false
tags: [cybersecurity,phishing]
categories: [""]
---

In this article, attackers and their use of psychology to bypass technical security measures will be investigated.

**Note**: Attempting to phish the credentials of someone without their express consent is illegal. The information presented in this course in no way encourages or condones phishing, and should not be used to attempt a phishing attack.

Phishing is one of the most well-known types of cyber attacks. The average internet user has never heard of Kali Linux or written Python scripts to guess passwords, but _almost_ everyone knows **not** to respond to an email from a down on their luck Nigerian Prince.

One of the reasons that phishing is so common is because **it works**! No matter what technical controls are in place to secure a system, humans within the system are still hackable. The practice of tricking humans to get important data or access is also known as social engineering.

Estimates range on how effective phishing is, but given that it can be used for everything from credential theft to loading malware in systems, Verizon labeled it [the biggest threat to small organizations in 2020](https://www.securitymagazine.com/articles/92415-verizon-2020-data-breach-report-money-still-makes-the-cyber-crime-world-go-round).

Sometimes phishing attacks can seem comically implausible, such as a [phishing email from an unfortunate astronaut lost in space](https://www.securitymagazine.com/articles/92415-verizon-2020-data-breach-report-money-still-makes-the-cyber-crime-world-go-round). However, with phishing attacks becoming both more common and more sophisticated, it’s vital to be able to identify and stop phishing attacks.

### Different Types of Phishing
All types of phishing rely on social engineering to get a victim to take some action, but there are different methods and targets beyond email, for example:
- [Vishing](https://us.norton.com/internetsecurity-online-scams-vishing.html) (from “voice phishing”), which refers to the spam calls in which an attacker claims to be from a victim’s bank or law enforcement and tries to extract information
- [Smishing](https://us.norton.com/internetsecurity-emerging-threats-what-is-smishing.html) (from “SMS phishing”) is when an attacker attempts to do the same thing over text message, by sending a malicious link
- [Webpages](#not-just-emails-webpages-that-steal-your-password), which we’ll discuss later on

Phishing is also categorized by who it targets. Many phishing campaigns send out mass spam emails to individuals and organizations, hoping to catch a victim in a wide net. But sometimes, an attacker has a specific target in mind and sends that target a dedicated, personalized email. This is known as [spear phishing](https://www.csoonline.com/article/3334617/what-is-spear-phishing-why-targeted-email-attacks-are-so-difficult-to-stop.html). If the target is extremely sought after, like the CEO of a company, it is known as [whaling](https://www.kaspersky.com/resource-center/definitions/what-is-a-whaling-attack).

Whether it is used to trick someone into sending money, to harvest login credentials, or to download malware, phishing targets humans as an initial attack vector.

### How Does Phishing Work?
Sometimes phishing attacks are just emails or phone calls that attempt to get a victim to send an attacker money or payment information. Others, such as those that get people to click on links that download malware onto their systems, require more technical finesse. For example, an attacker could:
1. Embed a PDF or Word document with malicious code
2. Attach it to a phishing email
3. Social engineer a user into downloading and opening it, executing the malicious code

Often, this malicious code contains the functionality to further spread the virus by sending more phishing emails to the user’s contacts.

### Email Spoofing
Email spoofing refers to when an attacker falsifies their email headers to make it appear as though the email is coming from someone else. Spoofing is a common component in phishing emails, used in as many as [90% of email fraud attacks](https://www.proofpoint.com/us/blog/threat-protection/email-fraud-continues-rise-number-attacks-grew-36-q2).

In a typical email, the “from” field is automatically filled out. If my email is `john_johnson[@]gmail[.]com`, and I send an email to my friend, my friend will see that the email came from my email address. However, one can also send emails with simple scripts (here are instructions for [sending an email in Python](https://realpython.com/python-send-email/)).

When an email is written and sent using a programming script, its headers can be configured to be anything - meaning that an attacker can put any email as the “sender”. In order to really see what is going on in an email, it can be downloaded and opened in a code editor, but most email providers allow to see the email headers from within the email client. For example, in Gmail, if an email of interest is opened, the three vertical dots in the upper right-hand corner, then “Show original”, thus the email headers can be seen.

These email headers provide valuable information that can help detect phishing, such as the “return-to” address, sender IP, and whether the email failed any protections such as [SPF and DKIM](https://www.csoonline.com/article/3254234/mastering-email-security-with-dmarc-spf-and-dkim.html), which help to fight spoofing (they are the reason emails are automatically sent to the spam folder). If a suspicious email is spotted, it is always wise to open the headers before responding in order to see if any protection fields “failed”, and to look at the original sender IP. More about email spoofing [here](https://www.proofpoint.com/us/glossary/email-spoofing).

![An image showing that many of the protection fields in this email failed](/images/8_failing_header.png)

The headers of a phishing email, showing that it failed SPF protections.

### Not Just Emails: Webpages That Steal Your Password
Webpages that harvest credentials are especially effective phishing tools. Because these pages often forward victims to a legitimate webpage after stealing their login information, the user never realizes they were phished. These webpages can also encourage the user to download malware unknowingly.

If someone were trying to steal Codecademy logins they could occupy a [typo-squatting](https://www.upguard.com/blog/typosquatting) domain like **codecademy.cm** or **codeoademy.com** in the hopes that a user would accidentally type in the wrong domain. A malicious actor could also disguise their domain with a link shortener like bitly to get someone to click through to a disguised domain.

Below, we’ve set up our own credential harvesting webpage that looks identical to the Codecademy login page. We did this by downloading the HTML files for Codecademy. In the screenshot, the page is on a local server we can monitor, but we could choose to host it on any domain we owned.

![An image showing a page that looks like Codecademy, but it isn't](/images/8_fake_codecademy.png)

When someone lands on this page, they think it’s the real codecademy.com! When they enter their username “admin” and password “password” and log in, that login information gets sent to our backend.

![An image showing that the backend of this fake page discovered that the username is probably "admin" and the password is probably "password"](/images/8_backend.png)

Because we can program the “Log in” button to redirect to the real Codecademy page, unless a potential victim had looked at the domain and noticed that something was off, they would have had no indication that the page just sent their information to us. This is one potential way that an attacker can use a website to trick someone into handing over their credentials. It seems like there are a million ways we can be expertly deceived on the web. Let’s talk about how to detect these!

Phishing is a type of  attack that can take many forms. For example, , (voice phishing), and  (SMS phishing) are all threats. Attackers can also send emails that look like they are from legitimate senders by using spoofing. 

### Detection Techniques
Fortunately, there are ways that we can train both ourselves and our organizations not to fall for phishing! Although many phishing websites are near copies of the originals, phishing emails can be easier to spot. Below are three examples of phishing, two emails and one webpage.

#### Example One:
![An image showing an email from Paypal. It's from paypal[.]accounts@gmail[.]com](/images/8_paypal_email.png)

This one is pretty convincing. The sender is `paypal[.]accounts@gmail[.]com`. Remember, anybody can register a `@gmail.com` address. The real PayPal will always use a business domain: `@paypal.com`. Another method? [Developer tools](https://www.codecademy.com/articles/use-devtools) can be opened on any buttons in an email to see where they lead. Developer tools is a cybersecurity expert’s best friend. It can reveal many secrets that attackers don’t want users to see.

#### Example Two:
![An image showing an email from Tom Atwood. Tom is asking if Laurie will open a link to read and review Tom's resume at goodle.com](/images/8_resume.png)

This is also a pretty convincing email, especially if Tom Atwood is listed among the contacts. In fact, once an attacker compromises an email address, they can use it to distribute more phishing emails to the people in the victim’s contacts list, utilizing email spoofing to make the emails appear to come from known contacts. The fact that the attacker addresses the victim by name would also make this an example of spear phishing. What’s the giveaway here? Take a closer look at that URL - the second “g” in “google” is really a “d”. This means that the link is probably taking to a malicious fake website which will ask the user to log in to Google Drive and steal their credentials.

#### Example Three:
![An image showing the Social Security website at ssa.com. There is a username and password field with a "Sign In" button](/images/8_SSN.png)

This webpage is very similar to the real Social Security webpage. However, two things are a bit off. Firstly, all official U.S. government websites should have a .gov domain, not a .com, and secondly, have you ever seen a username and password field without an option for “forgot password?” Looking at the domain will probably provide the best information, but paying attention to small details such as missing or malfunctioning buttons, and grammar or punctuation mistakes, are key in identifying phishing pages.
Multiple choice

What are some ways to spot phishing webpages?

### Conclusion and Further Reading
The variety of phishing types, the low cost to create phishing pages, and the ease with which someone can create one, all make phishing a difficult threat to counter. Additionally, regardless of how complex a system is, no system in the world can guarantee against a human employee clicking on a malicious link. This is why it is important to always report suspicious emails or links at work to the appropriate department so that they can block suspicious senders and domains. If small details are being noticed and reporting suspicious content, one person can do much to protect an organization and themselves against phishing attacks.

While we’ve talked about some strategies for phishing, they should never be used to harm others. If one is on the security team at work, they could run a phishing campaign to see if employees know what types of content to avoid.

#### Want to Learn More?
Certain tools like [Passive DNS](https://www.trendmicro.com/vinfo/us/security/news/cybercrime-and-digital-threats/infosec-guide-domain-monitoring-detecting-phishing-attacks) are useful to find real live phishing pages online, or check out [some real examples of phishing campaigns](https://www.phishing.org/phishing-examples). They can also be found in the email’s spam folder, and email headers can be examined to see where the mail is coming from, but be careful not to click on anything! Finally, check out [some famous phishing attacks](https://www.thesslstore.com/blog/the-dirty-dozen-the-12-most-costly-phishing-attack-examples/).

