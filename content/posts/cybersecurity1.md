---
title: "1. What Is Cybersecurity?"
date: 2023-01-01T18:56:00+01:00
draft: false
tags: [cybersecurity]
categories: [""]
---

### We’ll define what cybersecurity is, exactly!
As we become increasingly connected through the Internet by access through the computer, our phones, and even household devices, security has become a hot topic. Attacks can happen from any corner. What happens if someone gains control of someone else's computer, phone, or Google account?

On an enterprise level, attacks on computer systems can also breach millions of pieces of personal data, including credit card information. Governments are also vulnerable to attacks that expose sensitive data.

Cybersecurity involves everyone and every entity - each of us, our neighbors, organizations, companies, governments.

The Internet has completely revolutionized the way we communicate with each other and share information. Many of us spend hours on social media and online group chats. Nearly all institutions have some sort of computer system administration to keep track of accounts, and buying and selling things on the Internet is now the norm. One by one, even medical equipment, transportation systems, and vacuums are connected to the web.

## What is Cybersecurity, exactly?
There’s no turning back, even as more connections to the Internet lead to more privacy concerns and security risks. We don’t want strangers to access our accounts, or our credit card numbers. Along with practicing personal security, organizations and businesses also need to do their part to implement the right protections.

Cybersecurity is the field of study and practice that responds to these challenges as technology evolves. In a formal definition by [Cisco](https://www.cisco.com/c/en/us/products/security/what-is-cybersecurity.html):

    Cybersecurity is the practice of protecting systems, networks,
    and programs from digital attacks.

Digital attacks can cover a whole range from fraudulent emails to a targeted shutdown of a website’s traffic. Defenses against these attacks, then, must be learned and implemented at all levels.

### The CIA Triad
A basic, overarching model for cybersecurity, particularly as it relates to information, is the [CIA Triad](https://www.techtarget.com/whatis/definition/Confidentiality-integrity-and-availability-CIA). **CIA** stands for **C**onfidentiality, **I**ntegrity, and **A**vailability (not the US Central Intelligence Agency). Nearly all information security policies trace back to this model. Let’s go through each component of this triad.

![CIA Triad](/images/1_cia_triad.png)

#### Confidentiality
This pillar of the triad refers to protecting private information from eyes that shouldn’t have access to it. Confidentiality is the need to enforce access - who can see this, and who shouldn’t? For example, we don’t want to give our social security number to just anyone, but we trust that the institutions we give them to - like tax services - implement the right security measures to keep it secret. So what tools are used to guarantee the right access?

Some of the ways confidentiality is managed are:
- Keeping levels of access and setting permissions
- Encrypting data and files
- Requiring multi-factor authentication

#### Integrity
Integrity refers to data integrity here. We need security controls that protect data from being changed or deleted. We must also ensure that the damage can be reversed if data was changed accidentally or by the wrong person.

Some techniques related to integrity are:
- Keeping backups of the data in its correct state, and logging versions
- Using cryptography to securely check for changes
- Keeping track of digital signatures to prove integrity of data

#### Availability
This last pillar refers to data being consistently, reliably available to those authorized. For example, when someone logins to a social media account and want to set their privacy settings, they expect all the correct settings that had been set before to appear immediately. The social media company ensures that even with high traffic, information gets to the "right" screen. How is this accomplished?

- Always monitoring servers and networks
- Maintaining hardware and software
- Having a plan for disaster recovery

#### Non-repudiation
Assurance that someone/something cannot deny something (e.g. one cannot deny the authenticity of a digital signature). In [InfoSec, non-repudiation](https://en.wikipedia.org/wiki/Non-repudiation#In_digital_security) means:
- A service that provides proof of the integrity and origin of data
- An authentication that can be said to be genuine with high confidence
- An authentication that the data is available under specific circumstances, or for a period of time: data availability

### IT vs. OT
[Information technology (IT)](https://en.wikipedia.org/wiki/Information_technology) is the use of computers to create, process, store, retrieve, and exchange all kinds of data and information. IT forms part of [information and communications technology (ICT)](https://en.wikipedia.org/wiki/Information_and_communications_technology).
[Operational technology (OT)](https://en.wikipedia.org/wiki/Operational_technology) is hardware and software that detects or causes a change, through the direct monitoring and/or control of industrial equipment, assets, processes and events. The term has become established to demonstrate the technological and functional differences between traditional information technology (IT) systems and [industrial control systems (ICS)](https://en.wikipedia.org/wiki/Industrial_control_system) environment, the so-called "IT in the non-carpeted areas".

## And on we go!
Again, the CIA triad provides a foundation for information security specifically. We will cover many other security principles and frameworks, and explore techniques deeper.

In these notes, a foundational understanding of the field of cybersecurity will be shared, including:
- how cybersecurity has evolved, including how it’s shaped modern standards and regulations
- how digital attacks operate, and their different targets and motivations
- basic knowledge of cryptography and authentication and authorization technologies
- how to make our personal devices and networks more secure
- the anatomy of networks
- what cybersecurity looks like in the era of cloud technology

